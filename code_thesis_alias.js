cleos -u http://13.229.222.229:8881 get info
{
  "server_version": "60c8bace",
  "chain_id": "a5595b8d2fb38d8d075eed725918f08a9d9783bbdd56d619784e5e7ea43fa936",
  "head_block_num": 6551416,
  "last_irreversible_block_num": 6551372,
  "last_irreversible_block_id": "0063f74ce4750513ba44ea97ae63a17c53546412d307665bef0cf87e0be52bd1",
  "head_block_id": "0063f77831ab590161151e03c0eb82bb7d2c18342347fe375ff841b0e701c01f",
  "head_block_time": "2018-12-22T09:44:15.500",
  "head_block_producer": "accountnum12",
  "virtual_block_cpu_limit": "10000000000",
  "virtual_block_net_limit": 1048576000,
  "block_cpu_limit": 9999900,
  "block_net_limit": 1048576,
  "server_version_string": "v1.4.2-dirty"
}

cleos push action iotsc createprl '["quyvo" , "quyvo" , "university of science" , "0975449712" , "quyvok37@gmail.com" , "thua thien hue" , "iot sc test"]' -p quyvo

cleos push action iotsc createsensor '["quyvo" , "temperature" , "huong thuy, thua thien hue" , "description" , "2018-12-01T19:11:11"]' -p quyvo

cleos push action iotsc createsensor '["quyvo" , "humidity" , "quan 10, ho chi minh" , "description" , "2018-12-01T19:11:11"]' -p quyvo

cleos get table iotsc iotsc profile                                                                                                                                       1 ↵
{
  "rows": [{
      "owner": "quyvo",
      "name": "quyvo",
      "company": "university of science",
      "email": "quyvok37@gmail.com",
      "address": "thua thien hue",
      "telephone": "0975449712",
      "description": "iot sc test"
    }
  ],
  "more": false
}

cleos get table iotsc quyvo sensordetail
{
  "rows": [{
      "owner": "quyvo",
      "type": "temperature",
      "address": "huong thuy, thua thien hue",
      "description": "description",
      "start_time": "2018-12-01T19:11:11"
    }
  ],
  "more": false
}


cleos get table iotsc quyvo sensordata
{
  "rows": [{
      "type": "humidity",
      "value": 60,
      "id": "1536520157000000",
      "owner": "quyvo"
    },{
      "type": "humidity",
      "value": 62,
      "id": "1536520158000000",
      "owner": "quyvo"
    },{
      "type": "temperature",
      "value": 27,
      "id": "1536520159000000",
      "owner": "quyvo"
    },{
      "type": "temperature",
      "value": 28,
      "id": "1536520160000000",
      "owner": "quyvo"
    }
  ],
  "more": true
}

