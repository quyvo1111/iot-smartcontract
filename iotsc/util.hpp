#include <boost/algorithm/string.hpp>

namespace convertType{

  using std::string;

  uint64_t char_to_symbol( char c ) {
      if( c >= 'a' && c <= 'z' )
         return (c - 'a') + 6;
      if( c >= '1' && c <= '5' )
         return (c - '1') + 1;
      return 0;
   }

  string name_to_string(uint64_t value){
    static const char* charmap = ".12345abcdefghijklmnopqrstuvwxyz";

      string str(13,'.');

      uint64_t tmp = value;
      for( uint32_t i = 0; i <= 12; ++i ) {
         char c = charmap[tmp & (i == 0 ? 0x0f : 0x1f)];
         str[12-i] = c;
         tmp >>= (i == 0 ? 4 : 5);
      }

      boost::algorithm::trim_right_if( str, []( char c ){ return c == '.'; } );
      return str;
    }
   
  uint64_t string_to_name( const string strIn )
    {
        const char *str = strIn.c_str();
        const auto len = strnlen(str, 14);
        if(len >= 13)
            return -1;
        uint64_t name = 0;
        int i = 0;
        for ( ; str[i] && i < 12; ++i) {
            // NOTE: char_to_symbol() returns char type, and without this explicit
            // expansion to uint64 type, the compilation fails at the point of usage
            // of string_to_name(), where the usage requires constant (compile time) expression.
            name |= (char_to_symbol(str[i]) & 0x1f) << (64 - 5 * (i + 1));
        }

        // The for-loop encoded up to 60 high bits into uint64 'name' variable,
        // if (strlen(str) > 12) then encode str[12] into the low (remaining)
        // 4 bits of 'name'
        if (i == 12)
            name |= char_to_symbol(str[12]) & 0x0F;
        
        if(name_to_string(name) != string(str)){
            return -1;
        }
        return name;
    }
}