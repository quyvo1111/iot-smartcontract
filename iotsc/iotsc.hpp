#include <eosiolib/eosio.hpp>
#include <eosiolib/time.hpp>
#include <set>
#include <string>
#include "util.hpp"

#define table_def uint64_t id; \
  account_name owner; \
  auto primary_key() const { return id; }

namespace iotsc {

  using eosio::contract;
  using eosio::time_point;
  using eosio::time_point_sec;
  using eosio::milliseconds;
  using std::string;
  using convertType::name_to_string;
  using convertType::string_to_name;

  struct profile{
    account_name owner;
    string name;
    string company;
    string email;
    string address;
    string telephone;
    string description;
    auto primary_key() const { return owner; };
  };

  typedef eosio::multi_index<N(profile), profile> profiles;
  
  struct sensordetail{
    account_name owner;
    string type;
    string address;
    string description;
    time_point start_time;
    auto primary_key() const { return string_to_name(type.c_str()); };
  };

  typedef eosio::multi_index<N(sensordetail), sensordetail> sensordetails;

  struct sensordata {
    string type;
    uint64_t value;
    auto byType() const { return string_to_name(type.c_str()); }
    table_def;
  };

  typedef eosio::multi_index<N(sensordata), sensordata,
                           eosio::indexed_by< N( type ),
                           eosio::const_mem_fun< sensordata, uint64_t, &sensordata::byType > > > sensordatas;

  class iotsc : public contract {
      public:
         iotsc(account_name self);

         void check(string msg);

         void createprl(
                        account_name owner,
                        string name,
                        string company,
                        string telephone,
                        string email,
                        string address,
                        string description);
         
         void createsensor(
                        account_name owner,
                        string type,
                        string address,
                        string description,
                        time_point start_time);

         void save(account_name owner,
                   string type,
                   uint64_t value,
                   time_point timestamp);

         void del(account_name owner,
                  time_point from,
                  time_point to,
                  string type);

      private:

         profiles t_profiles;

         void initialize_contract();
   };



} /// namespace iotsc
