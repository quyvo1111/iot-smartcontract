#include "iotsc.hpp"

namespace iotsc{
  using eosio::print;

  iotsc::iotsc(account_name self): contract(self), t_profiles(self, self){
    initialize_contract();
  }

  void iotsc::check(string msg) {
    print("---- msg from hello: ", msg, "\n");
  }

  void iotsc::createprl(
                account_name owner,
                string name,
                string company,
                string telephone,
                string email,
                string address,
                string description){

                // check authentication of creator
    require_auth(owner);

    eosio_assert(!name.empty(), "name should not be empty!");

    auto pItr = t_profiles.find(owner);
    
    // check if timestamp is valid
    eosio_assert(pItr == t_profiles.end(), "sensor details with this timestamp is alrealy exist");

    // create
    t_profiles.emplace(owner, [&](auto &p) {
      p.owner = owner;
      p.name = name;
      p.company= company;
      p.description = description;
      p.email = email;
      p.address = address;
      p.telephone = telephone,
      p.description = description;
    });

  };

  void iotsc::createsensor(
                            account_name owner,
                            string type,
                            string address,
                            string description,
                            time_point start_time
                          ){
    require_auth(owner);

    sensordetails t_sensordetails(_self, owner);
    
    // check type rule is valid 
    eosio_assert(string_to_name(type) != -1, "Type should be less than 13 characters and only contains the following symbol .12345abcdefghijklmnopqrstuvwxyz");

    auto pItr = t_sensordetails.find(string_to_name(type));
    
    // check if timestamp is valid
    eosio_assert(pItr == t_sensordetails.end(), "sensor details with this timestamp is alrealy exist");

    // create
    t_sensordetails.emplace(owner, [&](auto &p) {
      p.type = type;
      p.address = address;
      p.owner = owner;
      p.description = description;
      p.start_time = start_time;
    });

  };

  void iotsc::save(account_name owner,
                   string type,
                   uint64_t value,
                   time_point timestamp){
        // check authentication of owner
    require_auth(owner);
      /**
      * sensordata: scope = owner; code = _self
      */
    const uint64_t id = timestamp.time_since_epoch().count();

    sensordatas t_sensordata(_self, owner);
    
    // check type rule is valid 
    eosio_assert(string_to_name(type) != -1, "Type should be less than 13 characters and only contains the following symbol .12345abcdefghijklmnopqrstuvwxyz");

    auto pItr = t_sensordata.find(id);
    
    // check if timestamp is valid
    eosio_assert(pItr == t_sensordata.end(), "sensor data with this timestamp is alrealy exist");

    // create
    t_sensordata.emplace(owner, [&](auto &p) {
      p.id = timestamp.time_since_epoch().count();
      p.type = type;
      p.value = value;
      p.owner = owner;
    });

  }

  void iotsc::del(account_name owner,
                  time_point from,
                  time_point to,
                  string type ){
    sensordatas t_sensordata(_self, owner);
    auto dataItr = t_sensordata.lower_bound(from.time_since_epoch().count());

    while(dataItr->id >= from.time_since_epoch().count() && dataItr->id \ 
                                  <= to.time_since_epoch().count()){
      print("sensor data:", "  ",dataItr->id,"  ", dataItr->type,"  ", dataItr->value,"  ", eosio::name{dataItr->owner});
      dataItr++;
    }
  };

  void iotsc::initialize_contract(){
    return;
  }
}

EOSIO_ABI(iotsc::iotsc, (save)(check)(del)(createprl)(createsensor))
